import pandas
import pandas as pd
import sklearn as sk
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.neighbors import KNeighborsRegressor
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.svm import SVR
from sklearn.metrics import r2_score
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from xgboost import XGBRegressor

pd.set_option("display.max_columns",100)
pd.set_option("display.width",1000)

df = pd.read_csv("company_sales.csv",sep=';', engine='python')
print(df)
print(df.describe())

print(df.count())
print("# of columns",len(df))
print(df.std())

# total profit area has null values as seen on describe command.
print("\n\nNumber of null Total Profit:", sum(pd.isnull(df['Total Profit'])))

boolian_null = pd.isnull(df["Total Profit"])
boolian_notnull = pd.notnull(df["Total Profit"])
dfnull = df[boolian_null]
#print (dfnull.describe())
dfnotnull =df[boolian_notnull]
#print (dfnotnull.describe())


#find correlation between total profit and other variables.
print(dfnotnull.corr(method ='pearson'))

# Plot Correlation

# Plot
plt.scatter(dfnotnull['Total Profit'],dfnotnull['Product Profit'] ,alpha=0.5)
plt.title('Total Profit vs Product Profit')
plt.xlabel('Total Profit')
plt.ylabel('Product Profit')
plt.show()

plt.scatter(dfnotnull['Total Profit'],dfnotnull['P1'] ,alpha=0.5)
plt.title('Total Profit vs P1')
plt.xlabel('Total Profit')
plt.ylabel('P1')
plt.show()

plt.scatter(dfnotnull['Total Profit'],dfnotnull['P8'] ,alpha=0.5)
plt.title('Total Profit vs Product P8')
plt.xlabel('Total Profit')
plt.ylabel('P8')
plt.show()

plt.scatter(dfnotnull['Total Profit'],dfnotnull['P17'] ,alpha=0.5)
plt.title('Total Profit vs P17')
plt.xlabel('Total Profit')
plt.ylabel('P17')
plt.show()

plt.scatter(dfnotnull['Total Profit'],dfnotnull['P16'] ,alpha=0.5)
plt.title('Total Profit vs P16')
plt.xlabel('Total Profit')
plt.ylabel('P16')
plt.show()

plt.scatter(dfnotnull['Total Profit'],dfnotnull['P13'] ,alpha=0.5)
plt.title('Total Profit vs P13')
plt.xlabel('Total Profit')
plt.ylabel('P13')
plt.show()

plt.scatter(dfnotnull['Total Profit'],dfnotnull['P8'] ,alpha=0.5)
plt.title('Total Profit vs P8')
plt.xlabel('Total Profit')
plt.ylabel('P8')
plt.show()

plt.scatter(dfnotnull['Total Profit'],dfnotnull['P5'] ,alpha=0.5)
plt.title('Total Profit vs P5')
plt.xlabel('Total Profit')
plt.ylabel('P5')
plt.show()

plt.scatter(dfnotnull['Total Profit'],dfnotnull['P6'] ,alpha=0.5)
plt.title('Total Profit vs P6')
plt.xlabel('Total Profit')
plt.ylabel('P6')
plt.show()

print("Standard Deviation",dfnotnull["Total Profit"].std())
print("Variance",dfnotnull["Total Profit"].var())
print("Median",dfnotnull["Total Profit"].median())
print("Mean",dfnotnull["Total Profit"].mean())
print("Maximum",dfnotnull["Total Profit"].max())
print("Minimum",dfnotnull["Total Profit"].min())

#appliying various algoritm for to predicting house prices
prediction_model_columns =['Product Profit','P1','P2','P3','P4','P5','P6','P7','P8','P9','P10','P11','P12','P13','P14','P15','P16','P17','P18','P19','P20']
Xtrain, Xtest, Ytrain, Ytest = train_test_split(dfnotnull[prediction_model_columns], dfnotnull[['Total Profit']],test_size=0.4)

models = [LinearRegression(),
        RandomForestRegressor(n_estimators=47, max_features='sqrt'),
        KNeighborsRegressor(n_neighbors=1),
        SVR(kernel='linear'),
        LogisticRegression(),
        XGBRegressor(n_estimator=50, max_depth=2)
        ]
TestModels = pd.DataFrame()
tmp = {}
for model in models:
    # get model name
    m = str(model)
    tmp['Model'] = m[:m.index('(')]
    # fit model on training dataset
    model.fit(Xtrain, Ytrain['Total Profit'])
    # predict prices for test dataset and calculate r^2
    tmp['R2_Total_Profit'] = r2_score(Ytest['Total Profit'], model.predict(Xtest))
    # write obtained data
    TestModels = TestModels.append([tmp])
TestModels.set_index('Model', inplace=True)

fig, axes = plt.subplots(ncols=1, figsize=(10, 4))
TestModels.R2_Total_Profit.plot(ax=axes, kind='bar', title='R2_Total_Profit')
plt.show()

# run models with columns which have correlation value with total profit above 0.1 end below -0.1
prediction_model_columns =['P16','P17','P13','P8','P5','P6','Product Profit','P1']
Xtrain, Xtest, Ytrain, Ytest = train_test_split(dfnotnull[prediction_model_columns], dfnotnull[['Total Profit']],test_size=0.4)

models = [LinearRegression(),
        RandomForestRegressor(n_estimators=47, max_features='sqrt'),
        KNeighborsRegressor(n_neighbors=1),
        SVR(kernel='linear'),
        LogisticRegression(),
        XGBRegressor(n_estimator=50, max_depth=2)
        ]
TestModels = pd.DataFrame()
tmp = {}
for model in models:
    # get model name
    m = str(model)
    tmp['Model'] = m[:m.index('(')]
    # fit model on training dataset
    model.fit(Xtrain, Ytrain['Total Profit'])
    # predict prices for test dataset and calculate r^2
    tmp['R2_Total_Profit'] = r2_score(Ytest['Total Profit'], model.predict(Xtest))
    # write obtained data
    TestModels = TestModels.append([tmp])
TestModels.set_index('Model', inplace=True)

fig, axes = plt.subplots(ncols=1, figsize=(10, 4))
TestModels.R2_Total_Profit.plot(ax=axes, kind='bar', title='R2_Total_Profit_FS_Corr')
plt.show()

# Get numerical feature importances
rf=RandomForestRegressor(n_estimators=47, max_features='sqrt')
rf.fit(Xtrain, Ytrain['Total Profit'])
importances = list(rf.feature_importances_)
# List of tuples with variable and importance
feature_importances = [(feature, round(importance, 2)) for feature, importance in zip(prediction_model_columns, importances)]
# Sort the feature importances by most important first
feature_importances = sorted(feature_importances, key = lambda x: x[1], reverse = True)
# Print out the feature and importances
[print('Variable: {:20} Importance: {}'.format(*pair)) for pair in feature_importances];

# run models with columns which have importance value above 0.05
prediction_model_columns =['Product Profit','P10','P4','P5','P11','P20','P1']
Xtrain, Xtest, Ytrain, Ytest = train_test_split(dfnotnull[prediction_model_columns], dfnotnull[['Total Profit']],test_size=0.4)

models = [LinearRegression(),
        RandomForestRegressor(n_estimators=47, max_features='sqrt'),
        KNeighborsRegressor(n_neighbors=1),
        SVR(kernel='linear'),
        LogisticRegression(),
        XGBRegressor(n_estimator=50, max_depth=2)
        ]
TestModels = pd.DataFrame()
tmp = {}
for model in models:
    # get model name
    m = str(model)
    tmp['Model'] = m[:m.index('(')]
    # fit model on training dataset
    model.fit(Xtrain, Ytrain['Total Profit'])
    # predict prices for test dataset and calculate r^2
    tmp['R2_Total_Profit'] = r2_score(Ytest['Total Profit'], model.predict(Xtest))
    # write obtained data
    TestModels = TestModels.append([tmp])
TestModels.set_index('Model', inplace=True)

fig, axes = plt.subplots(ncols=1, figsize=(10, 4))
TestModels.R2_Total_Profit.plot(ax=axes, kind='bar', title='R2_Total_Profit_FS_Importance')
plt.show()

# run models with columns which have importance value above 0.05 and correlation value with total profit above 0.1 end below -0.1
prediction_model_columns =['Product Profit','P1','P5']
Xtrain, Xtest, Ytrain, Ytest = train_test_split(dfnotnull[prediction_model_columns], dfnotnull[['Total Profit']],test_size=0.4)

models = [LinearRegression(),
        RandomForestRegressor(n_estimators=47, max_features='sqrt'),
        KNeighborsRegressor(n_neighbors=1),
        SVR(kernel='linear'),
        LogisticRegression(),
        XGBRegressor(n_estimator=50, max_depth=2)
        ]
TestModels = pd.DataFrame()
tmp = {}
for model in models:
    # get model name
    m = str(model)
    tmp['Model'] = m[:m.index('(')]
    # fit model on training dataset
    model.fit(Xtrain, Ytrain['Total Profit'])
    # predict prices for test dataset and calculate r^2
    tmp['R2_Total_Profit'] = r2_score(Ytest['Total Profit'], model.predict(Xtest))
    # write obtained data
    TestModels = TestModels.append([tmp])
TestModels.set_index('Model', inplace=True)

fig, axes = plt.subplots(ncols=1, figsize=(10, 4))
TestModels.R2_Total_Profit.plot(ax=axes, kind='bar', title='R2_Total_Profit_FS_Importance_and_Corr')
plt.show()

#sum of all sales attributes to find out if there is meaningfull relationship
#between total sales and total profit
col_list= list(dfnotnull)
col_list.remove('Total Profit')
col_list.remove('Product Profit')
col_list.remove('ï»¿Name Surname')
col_list.remove('ID')
#print(col_list)

dfnotnull['sum_of_sales'] = dfnotnull[col_list].sum(axis=1)
#print(dfnotnull.head())

prediction_model_columns =['Product Profit','sum_of_sales']
Xtrain, Xtest, Ytrain, Ytest = train_test_split(dfnotnull[prediction_model_columns], dfnotnull[['Total Profit']],test_size=0.4)

models = [LinearRegression(),
        RandomForestRegressor(n_estimators=47, max_features='sqrt'),
        KNeighborsRegressor(n_neighbors=1),
        SVR(kernel='linear'),
        LogisticRegression(),
        XGBRegressor(n_estimator=50, max_depth=2)
        ]
TestModels = pd.DataFrame()
tmp = {}
for model in models:
    # get model name
    m = str(model)
    tmp['Model'] = m[:m.index('(')]
    # fit model on training dataset
    model.fit(Xtrain, Ytrain['Total Profit'])
    # predict prices for test dataset and calculate r^2
    tmp['R2_Total_Profit'] = r2_score(Ytest['Total Profit'], model.predict(Xtest))
    # write obtained data
    TestModels = TestModels.append([tmp])
TestModels.set_index('Model', inplace=True)

fig, axes = plt.subplots(ncols=1, figsize=(10, 4))
TestModels.R2_Total_Profit.plot(ax=axes, kind='bar', title='R2_Total_Profit_Sum_of_Sales')
plt.show()

#none of the feature selection method does not result positive r2 value. So mean value of Total
#Profit make more sense for null values instead of prediction wiht regression.

df['Total Profit'].fillna(value=np.mean(df['Total Profit']), inplace=True,downcast=float)

print("\n\nNumber of null Total Profit:", sum(pd.isnull(df['Total Profit'])))